/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionesocketudp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
/**
 *
 * @author gianl
 */
public class Client {
    
    int serverPort;
    InetAddress serverIP;
    DatagramSocket dataSocket;
    DatagramPacket inPacket;
    DatagramPacket outPacket;
    byte[] buffer;
    String toSend;
    String toReceive;
    String curDate;
    
    public Client(int port, InetAddress IP){
        try {
            serverPort = port;
            serverIP = IP;
            dataSocket = new DatagramSocket();
        } catch (SocketException ex) {
            System.err.print(ex);
        }
    }
    
    public void write(String msg){
        try {
            toSend = msg;
            outPacket = new DatagramPacket(toSend.getBytes(), toSend.length(), serverIP, serverPort);
            dataSocket.send(outPacket);
        } catch (IOException ex) {
            System.err.print(ex);
        }
    }
    
    public String read(){
        try {
            buffer = new byte[256];
            inPacket = new DatagramPacket(buffer, buffer.length);
            dataSocket.receive(inPacket);
            toReceive = new String(inPacket.getData(), 0, inPacket.getLength());
            return toReceive;
        } catch (IOException ex) {
            System.err.print(ex);
            return null;
        }
    }
    
    public void fetch(){
        try {
            buffer = new byte[256];
            inPacket = new DatagramPacket(buffer, buffer.length);
            dataSocket.receive(inPacket);
            curDate = new String(inPacket.getData(), 0, inPacket.getLength());
            System.out.println("[*] Server date: "+ curDate);
        } catch (IOException ex) {
            System.err.print(ex);
        }
    }
    
    public void chiudi(){
        dataSocket.close();
    }
}
