/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionesocketudp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;

/**
 *
 * @author gianl
 */
public class Server {
    
    int port;
    InetAddress IP;
    int lastClientPort;
    InetAddress lastClientIP;
    DatagramSocket dataSocket;
    DatagramPacket inPacket;
    DatagramPacket outPacket;
    byte[] buffer;
    String toSend;
    String toGet;
    
    public Server(int port){
        try {
            IP = InetAddress.getLocalHost();
            this.port = port;
            dataSocket = new DatagramSocket(this.port);
            System.out.println("[*] Server listening: " + port);
        } catch (UnknownHostException ex) {
            System.err.print(ex);
        } catch (SocketException ex) {
            System.err.print(ex);
        }
    }
    
    public void write(String msg){
        try {
            toSend = msg;
            outPacket = new DatagramPacket(toSend.getBytes(), toSend.length(), lastClientIP, lastClientPort);
            dataSocket.send(outPacket);
        } catch (IOException ex) {
            System.err.print(ex);
        }
    }
    
    public void send(){
        try {
            Date d = new Date();
            outPacket = new DatagramPacket(d.toString().getBytes(), d.toString().length(), lastClientIP, lastClientPort);
            dataSocket.send(outPacket);
        } catch (IOException ex) {
            System.err.print(ex);
        }
    }
    
    public String read(){
        try {
            buffer = new byte[256];
            inPacket = new DatagramPacket(buffer, buffer.length);
            dataSocket.receive(inPacket);
            toGet = new String(inPacket.getData(), 0, inPacket.getLength());
            lastClientIP = inPacket.getAddress();
            lastClientPort = inPacket.getPort();
            System.out.println("[*] Client " + lastClientIP + " :" + lastClientPort + " sent a message: " + toGet);
            return toGet;
        } catch (IOException ex) {
            System.err.print(ex);
            return null;
        }
    }
    
    public void kill(){
        dataSocket.close();
    }
}
